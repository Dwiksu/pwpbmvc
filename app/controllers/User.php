<?php

class User extends Controller {
    
    public function index()
    {
        $data['title'] = 'Login';
        $data['users'] = $this->model('UserModel');

        $this->view('login/index', $data);
    }

    public function logout()
    {
        session_unset();
        session_destroy();
        header("Location: ?url=User/index");
    }

    public function register()
    {
        $data['users'] = $this->model('UserModel');
        $d = new UserModel;
        $f = new Flasher;

        if(isset($_POST['register']))
        {
            $r = $d->register($_POST);
            header("Location: ?url=User/index");
            if($r > 0) 
            {
                $f->setFlash('Register Berhasil', 'Register', 'success');
            }
            else
            {
                $f->setFlash('Register Gagal', 'Register', 'danger');
            }
            exit;
        }
    }

    public function loginpost()
    {
        $data['users'] = $this->model('UserModel');
        $d = new UserModel;
        $f = new Flasher;

        if(isset($_POST['login']))
        {
            $r = $d->login($_POST);
            if($r > 0) 
            {
                $f->setFlash('Login Berhasil', 'Login', 'success');
            }
            else
            {
                $f->setFlash('Login Gagal', 'Login', 'danger');
            }
            header("Location: ?");
            exit;
        }
    }
}