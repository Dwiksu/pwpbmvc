<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MVC | <?= $data['title']; ?></title>
    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/sign-in/">
    <!-- Bootstrap core CSS -->
    <link href="http://localhost/pwpbmvc/public/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/5.0.0/mdb.min.css" rel="stylesheet"/>

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    <!-- Custom styles for this template -->
    <link href="http://localhost/pwpbmvc/public/assets/css/signin.css" rel="stylesheet">
</head>
<body class="text-center">

<img class="mb-4" src="http://localhost/pwpbmvc/public/assets/img/bootstraplogo.png " alt="" width="220" height="170">

    <!-- Pills navs -->
<ul class="nav nav-pills mb-3" id="user" role="tablist">
<li class="nav-item" role="presentation">
    <a class="nav-link active" id="user-tab-2" data-mdb-toggle="pill" href="#user-login" role="tab" aria-controls="user-login" aria-selected="false">Login</a>
  </li>
  <li class="nav-item" role="presentation">
    <a class="nav-link" id="user-tab-1" data-mdb-toggle="pill" href="#user-register" role="tab" aria-controls="user-register" aria-selected="true">Register</a>
  </li>
</ul>

<?= Flasher::Flash() ?>

<div class="tab-content" id="user-content">
  <div class="tab-pane fade show active" id="user-login" role="tabpanel" aria-labelledby="user-tab-1">
        <form method="POST" action="?url=User/loginpost">
            <div class="form-floating">
                <input type="text" class="form-control" id="floatingInput" placeholder="Username" name="username">
                <label for="floatingInput">Username</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control" id="floatingInput" placeholder="Password" name="password">
                <label for="floatingInput">Password</label>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit" name="login">Log in</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2017–2021</p>
        </form>
  </div>
  <div class="tab-pane fade" id="user-register" role="tabpanel" aria-labelledby="user-tab-2">
  <main class="form-signin">
        <form action="?url=User/register" method="POST">
            <div class="form-floating">
                <input type="text" class="form-control" id="floatingInput" placeholder="Username" name="username">
                <label for="floatingInput">Username</label>
            </div>
            <div class="form-floating">
                <input type="text" class="form-control" id="floatingInput" placeholder="First Name" name="firstname">
                <label for="floatingInput">First name</label>
            </div>
            <div class="form-floating">
                <input type="text" class="form-control" id="floatingInput" placeholder="Last Name" name="lastname">
                <label for="floatingInput">Last name</label>
            </div>
            <div class="form-floating">
                <input type="email" class="form-control" id="floatingInput" placeholder="Email" name="email">
                <label for="floatingInput">Email</label>
            </div>
            <div class="form-floating">
                <input type="password" class="form-control" id="floatingInput" placeholder="Password" name="password">
                <label for="floatingInput">Password</label>
            </div>
            <button class="w-100 btn btn-lg btn-primary" type="submit" name="register">Sign in</button>
            <p class="mt-5 mb-3 text-muted">&copy; 2017–2021</p>
        </form>
    </main>
  </div>
</div>

<!-- MDB -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/5.0.0/mdb.min.js"></script>
</body>
</html>