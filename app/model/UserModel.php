<?php
class UserModel {
    private $table = 'users';
    private $db;

    private $name = 'Dwiksu';
    public function getUser()
    {
        return $this->name;
    }

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getAllUser()
    {
        $this->db->query("SELECT * FROM ($this->table)");
        return $this->db->resultAll();
    }

    public function getUserById($id)
    {
        $this->db->query("SELECT * FROM ($this->table) WHERE id=$id");
        $this->db->bind('id', $id);
        return $this->db->resultSingle();
    }

    public function Register($data)
    {
        $username = $data['username'];
        $firstname = $data['firstname'];
        $lastname = $data['lastname'];
        $email = $data['email'];
        $password = $data['password'];
        $password = password_hash($password, PASSWORD_DEFAULT);

        $this->db->query("INSERT INTO `$this->table` (`id`, `username`, `email`, `first_name`, `last_name`, `password`, `token`) VALUES (NULL,'$username','$email','$firstname','$lastname','$password',NULL)");
        return $this->db->affectedRow();
    }

    public function Login($data)
    {
        $username = $data['username'];
        $password = $data['password'];

        $this->db->query("SELECT * FROM ($this->table) WHERE `username` = '$username'");
        $user = $this->db->resultSingle();

        if($user === FALSE)
        {
            echo '<script>alert("Username salah!");</script>';
            header("Refresh: 0.01");
            die;
        } 
        elseif($user['username'] === $username)
        {
            if(password_verify($password, $user['password']))
            {
                $_SESSION['userID'] = $user['id'];
                return $this->db->affectedRow();
            }
        }
    }
}
?>